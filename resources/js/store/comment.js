import axios from 'axios'

export default {
    namespaced:true,
    state:{
        allcomments:[]
    },
    getters:{

    },
    mutations:{

    },
    actions:{
        async createComments({dispatch},comment){
            console.log('creando nueva comentario')
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/comments',comment)
        },
        meComment({commit},data){
        }
    }
}
